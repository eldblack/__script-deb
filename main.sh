#!/bin/bash
#!/bin/zsh
#

var="x"

menu(){
    while true $var != "x"
    do
    clear
    cat src/figlet.txt
    cat src/info.txt
    sleep 1
    echo ""
    cat src/menu.txt
    echo ""
    echo ">" 
    read var

    case "$var" in
        1)
            cp src/sources.list /etc/apt/sources.list
            sleep 1
            apt-get update
            apt-get install -f
            apt-get upgrade
            echo "____________| DONE |____________"
            sleep 3
;;
        2)
            apt-get install sudo
            apt-get install firmware-atheros
            apt-get install firmware-intel-sound
            apt-get install firmware-iwlwifi
            apt-get install firmware-linux-free
            apt-get install firmware-realtek
            apt-get install firmware-samsung
            apt-get install htop
            apt-get install unrar
            apt-get install zip
            apt-get install curl
            apt-get install wget
            apt-get install neofetch
            echo "____________| DONE |____________"
            sleep 3
;;
        3)
            sudo apt-get install zsh
            sudo apt-get install curl
            sudo apt-get install wget
            sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
            exit
            chsh -s $(which zsh)
            echo "____________| DONE |____________"
            sleep 3
;;
        4)
            apt-get --no-install-recommends install -y xserver-xorg-core
            apt-get --no-install-recommends install -y xserver-xorg-video-intel
            apt-get --no-install-recommends install -y xserver-xorg-video-nvidia
            apt-get --no-install-recommends install -y xserver-xorg-video-ati
            apt-get --no-install-recommends install -y xserver-xorg-video-fbdev
            apt-get --no-install-recommends install -y xserver-xorg
            sudo apt-get install apt-listbugs
#           Xfce4
            apt-get --no-install-recommends install -y xfwm4
            apt-get --no-install-recommends install -y xfce4-session
            apt-get --no-install-recommends install -y xfce4-terminal
            apt-get --no-install-recommends install -y xfce4-panel
            apt-get --no-install-recommends install -y xfce4-notifyd
            apt-get --no-install-recommends install -y xfce4-power-manager
            apt-get --no-install-recommends install -y xfce4-taskmanager
            apt-get --no-install-recommends install -y xfce4-settings
            apt-get --no-install-recommends install -y xfdesktop4
            apt-get --no-install-recommends install -y policykit-1
            apt-get --no-install-recommends install -y x11-xserver-utils
            apt-get --no-install-recommends install -y lightdm
            apt-get --no-install-recommends install -y lightdm-gtk-greeter-settings
            apt-get --no-install-recommends install -y thunar
            apt-get --no-install-recommends install -y mousepad
            apt-get --no-install-recommends install -y nomacs
            apt-get --no-install-recommends install -y blueman
            apt-get --no-install-recommends install -y network-manager-gnome
            apt-get --no-install-recommends install -y xarchiver
            apt-get --no-install-recommends install -y arc-theme
            apt-get --no-install-recommends install -y papirus-icon-theme
            apt-get --no-install-recommends install -y pulseaudio
            apt-get install pulseaudio-module-bluetooth
            apt-get install arc-theme
            echo "____________| DONE |____________"
            sleep 3
;;
        5)
            apt-get --no-install-recommends install -y xfce4-power-manager-plugins
            apt-get --no-install-recommends install -y xfce4-battery-plugin
            apt-get --no-install-recommends install -y xfce4-places-plugin
            apt-get --no-install-recommends install -y xfce4-timer-plugin
            apt-get --no-install-recommends install -y xfce4-xkb-plugin
            apt-get --no-install-recommends install -y xfce4-indicator-plugin
            apt-get --no-install-recommends install -y thunar-archive-plugin
            apt-get --no-install-recommends install -y thunar-media-tags-plugin
            apt-get --no-install-recommends install -y xfce4-whiskermenu-plugin
            apt-get --no-install-recommends install -y xfce4-pulseaudio-plugin
            echo "____________| DONE |____________"
            sleep 3
;;
        6)
            sudo apt-get update
            sudo apt-get install -f
            sudo apt-get --no-install-recommends install -y gnome-screenshot
            sudo apt-get install firefox
            sudo apt-get install menulibre
            sudo apt-get install vlc
            sudo apt-get --no-install-recommends install -y okular
            sudo apt-get --no-install-recommends install -y gnome-calculator
            sudo apt-get --no-install-recommends install -y gnome-clocks
            echo "____________| DONE |____________"
            sleep 3
;;
        7)
            sudo apt-get update
            sudo apt-get upgrade
            sudo apt-get install software-properties-common
            wget -q -O - https://repo.protonvpn.com/debian/public_key.asc | sudo apt-key add - 
            sudo add-apt-repository 'deb https://repo.protonvpn.com/debian unstable main'
            sudo apt-get update && sudo apt-get install protonvpn
            echo "____________| DONE |____________"
            sleep 3
;;
        8)
            clear
            cat src/info.txt
            sleep 3
;;
        0)
            echo "Exit..."
            sleep 4
            clear;
            exit;
;;
        *)
            echo "INVALID !!"

esac
done
}
menu
